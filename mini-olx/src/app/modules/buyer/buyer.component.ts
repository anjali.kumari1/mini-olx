import { Component, OnInit } from '@angular/core';
import {form_details} from '../form-details';

@Component({
  selector: 'app-buyer',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.css']
})
export class BuyerComponent implements OnInit {
  form_details=form_details;
  productSearch:string='';
  constructor() { }

  ngOnInit(): void {
  }

}
