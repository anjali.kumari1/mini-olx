import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';
import { SharedComponent } from './shared/shared.component';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {ModulesRoutingModule} from './modules-routing.module';
import { MatCardModule } from '@angular/material/card';
import { SearchPipe } from './pipes/search.pipe';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [BuyerComponent, SellerComponent, SharedComponent, SearchPipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MatCardModule,
    ModulesRoutingModule
  ],
  exports:[
    BuyerComponent,
    SellerComponent,
    SharedComponent
  ]
})
export class ModulesModule { }
