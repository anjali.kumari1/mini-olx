import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { form_details} from '../form-details';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.css']
})
export class SellerComponent implements OnInit {
  
  form = new FormGroup({
    product:new FormControl('',Validators.required),
    location:new FormControl(''),
    contactNumber:new FormControl('',
    [Validators.required,
    Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
    howOld:new FormControl(''),
    rating:new FormControl('',Validators.max(5)),
    yearOfPurchase:new FormControl('',Validators.required),
    costPrice:new FormControl(''),
    sellingPrice:new FormControl('')
    
  });

  constructor(private router:Router,) { 
  }
// get product(){
//   return this.form.get('product');
// }
textColor(){
  const cp=this.form.get('costPrice').value;
  const sp=this.form.get('sellingPrice').value;
  const diff=(Math.abs(cp-sp)/sp)*100;
  if(diff>10 && diff <20)
    return 'orange';
  else if(diff>20)
    return 'red';
  else
  return 'green';
}
  ngOnInit(): void {
  }
  
  onSubmit(){
    alert("Thank you for adding");
    form_details.push(this.form.value);
    console.log(form_details);
   this.router.navigateByUrl('/dashboard');
  }
}
