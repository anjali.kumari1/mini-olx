import { Component, OnInit } from '@angular/core';
import {categories} from '../categories';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit {
  categories=categories;
  constructor() { }

  ngOnInit(): void {
  }

}
