import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SellerComponent} from './seller/seller.component';
import {BuyerComponent} from './buyer/buyer.component';

const routes: Routes = [
  {
    path: 'seller',
    component: SellerComponent
  },{
    path: 'dashboard',
    component: BuyerComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
