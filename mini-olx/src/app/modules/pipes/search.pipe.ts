import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, productSearch:any){
    if(value.length===0)
    return value;
    return value.filter(function(search){
      return search.name_of_product.toLowerCase().indexOf(productSearch.toLowerCase())>-1
    });
  }

}
